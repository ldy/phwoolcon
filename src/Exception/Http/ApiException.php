<?php

/**
 * API异常处理
 */

namespace Phwoolcon\Exception\Http;

use Phwoolcon\ErrorCodes;
use Throwable;

class ApiException extends \Exception
{
    public function __construct($message = '', int $code = -1, Throwable $previous = null)
    {
        if (empty($message)) {
            $errorCode = ErrorCodes::getCodeMsg($code);
            $message = ($errorCode && count($errorCode) == 2) ? $errorCode[1] : '';
        }
        parent::__construct($message, $code, $previous);
    }

    public function getHeaders()
    {
        return [
            'content-type: application/vnd.api+json',
            'exception-type: ApiException',
        ];
    }

    public function getBody()
    {
        return json_encode([
            'jsonapi' => ['version' => '1.0'],
            'error' => $this->getCode(),
            'error_reason' => $this->getMessage()
        ]);
    }
}
