<?php

namespace Phwoolcon\Util;

class Common
{

    /**
     * 是否为json
     * @param $string
     * @param bool $strip_bom
     * @return bool
     */
    public static function is_json($string, $strip_bom = true)
    {
        if ($strip_bom) json_decode(trim($string, chr(239) . chr(187) . chr(191))); else json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * 是否为xml
     * @param $xml
     * @return bool
     */
    public static function is_xml($xml)
    {
        $xml_parser = xml_parser_create();
        if (!xml_parse($xml_parser, $xml, true)) {
            xml_parser_free($xml_parser);
            return false;
        } else {
            return true;
        }
    }

    /**
     *  XML解码
     * @param $xml
     * @return bool|mixed
     */
    public static function xml_decode($xml)
    {
        if (!$xml) {
            return false;
        }
        // 检查xml是否合法
        $xml_parser = xml_parser_create();
        if (!xml_parse($xml_parser, $xml, true)) {
            xml_parser_free($xml_parser);
            return false;
        }
        libxml_disable_entity_loader(true); //禁止引用外部xml实体
        //将XML转为array
        return json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    }

    /**
     * 数组过滤
     *
     * @param $filter
     * @param $data
     * @return array
     */
    public static function array_map_recursive($filter, $data)
    {
        $result = array();
        foreach ($data as $key => $val) {
            $result[$key] = is_array($val) ? self::array_map_recursive($filter, $val) : call_user_func($filter, $val);
        }
        return $result;
    }
}
