<?php

namespace Phwoolcon\Crypt;

class AES implements Crypt
{

    /**
     * 加密
     * @param string $str
     * @param $key
     * @return string
     */
    static function encrypt($str, $key)
    {
        //AES, 256 cbc模式加密数据
        $encrypt_str = openssl_encrypt($str, 'aes-256-cbc', base64_decode($key), OPENSSL_RAW_DATA, base64_decode(KeyConfig::$iv));
        return base64_encode($encrypt_str);
    }

    /**
     * 解密
     * @param string $str
     * @param $key
     * @return string
     */
    static function decrypt($str, $key)
    {
        //AES, 256 cbc模式解密数据
        $str = base64_decode($str);
        return openssl_decrypt($str, 'aes-256-cbc', base64_decode($key), OPENSSL_RAW_DATA, base64_decode(KeyConfig::$iv));
    }

    /**
     * 填充算法(chr()用于返回ASCII指定的字符，如chr(97)返回a)
     * @param string $source
     * @return string
     */
    static function addPKCS7Padding($source)
    {
        $source = trim($source);
        $block = mcrypt_get_block_size('rijndael-128', 'cbc');
        $pad = $block - (strlen($source) % $block);
        if ($pad <= $block) {
            $char = chr($pad);
            $source .= str_repeat($char, $pad);
        }
        return $source;
    }

    /**
     * 移去填充算法(ord()函数用于返回一个字符的ASCII值)
     * @param string $source
     * @return string
     */
    static function stripPKSC7Padding($source)
    {
        $char = substr($source, -1);
        $num = ord($char);
        $source = substr($source, 0, -$num);
        return $source;
    }

    /**
     * aes128加密
     * @param mixed $data 数据
     * @param string $key 加密key
     * @param string $iv 加密向量
     * @return string
     * @author wangyu <wangyu@ledouya.com>
     * @createTime 2018/5/30 14:23
     */
    public static function aes128Encrypt($data, $key, $iv)
    {
        if (is_array($data)) {
            $encrypt_data = json_encode($data);
        } elseif (is_object($data)) {
            $encrypt_data = json_encode($data, JSON_FORCE_OBJECT);
        } else {
            $encrypt_data = $data;
        }
        $encrypted = openssl_encrypt($encrypt_data, 'aes-128-cbc', $key, OPENSSL_RAW_DATA, $iv);
        return base64_encode($encrypted);
    }

    /**
     * aes128解密
     * @param string $data 字符串
     * @param string $key 加密key
     * @param string $iv 加密向量
     * @return string
     * @author wangyu <wangyu@ledouya.com>
     * @createTime 2018/5/30 14:23
     */
    public static function aes128Decrypt($data, $key, $iv)
    {
        return rtrim(openssl_decrypt(base64_decode($data), 'aes-128-cbc', $key, OPENSSL_RAW_DATA, $iv));
    }

}
