<?php
/**
 * Created by IntelliJ IDEA.
 *
 * 加解密算法
 *
 * User: yeran
 * Date: 2018/1/8
 * Time: 上午1:46
 */

namespace Phwoolcon\Crypt;


class Secret{


    public static function encrypt($way,$str,$key){

        switch ($way){
            case 'AES':{
                return AES::encrypt($str,$key);
            }

            case 'MD5':{
                return md5($str);
            }

            default:
                return AES::encrypt($str,$key);
        }

    }

    public static function decrypt($way,$str,$key){
        switch ($way){
            case 'AES':{
                return AES::decrypt($str,$key);
            }

            default:
                return AES::decrypt($str,$key);
        }
    }


}
