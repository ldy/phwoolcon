<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2018/1/8
 * Time: 上午1:51
 */


namespace Phwoolcon\Crypt;

interface Crypt{

    public static function encrypt($str,$key);

    public static function decrypt($str,$key);

}
